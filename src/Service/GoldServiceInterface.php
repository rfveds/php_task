<?php
/**
 * GoldServiceInterface.
 */

namespace App\Service;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Interface GoldServiceInterface
 */
interface GoldServiceInterface
{
    /**
     * GoldServiceInterface constructor.
     *
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client);

    /**
     * Get gold prices from http://api.nbp.pl/api/cenyzlota/{startDate}/{endDate}
     *
     * @param string $from
     * @param string $to
     * @return array
     */
    public function getGoldPrices(string $from, string $to): array;

    /**
     * Get gold prices from http://api.nbp.pl/api/cenyzlota/{startDate}/{endDate}
     *
     * @param string $from
     * @param string $to
     * @return array
     */
    public function getGoldPricesFromAPI(string $from, string $to): array;

}