<?php
/**
 * GoldService.
 */

namespace App\Service;

use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class GoldService
 */
class GoldService implements GoldServiceInterface
{
    /**
     * Cache
     *
     * @var FilesystemAdapter
     */
    private FilesystemAdapter $cache;

    /**
     * GoldService constructor.
     *
     * @param HttpClientInterface $client
     */
    public function __construct(private readonly HttpClientInterface $client)
    {
        $this->cache = new FilesystemAdapter();
    }

    /**
     * Get gold prices
     *
     * @param string $from
     * @param string $to
     * @return array
     * @throws InvalidArgumentException
     */
    function getGoldPrices(string $from, string $to): array
    {
        $cache_key = $from . $to;
        return $this->cache->get($cache_key, function (ItemInterface $item) use ($to, $from) {
            $item->expiresAfter(3600);
            return $this->getGoldPricesFromAPI($from, $to);
        });
    }

    /**
     * Get gold prices from http://api.nbp.pl/api/cenyzlota/{startDate}/{endDate}
     *
     * @param string $from
     * @param string $to
     * @return array
     * @throws Exception
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function getGoldPricesFromAPI(string $from, string $to): array
    {
        // Assert that 'from' and 'to' dates have time zone information
        // in the format of ±HH:MM or Z, if not respond with 400 Bad Request
        if (!preg_match('/[+-][0-9]{2}:[0-9]{2}|Z/', $from) || !preg_match('/[+-][0-9]{2}:[0-9]{2}|Z/', $to)) {
            throw new Exception('Date must have time zone information in the format of ±HH:MM or Z');
        }

        // Convert date strings to DateTime objects
        $from = new \DateTime($from);
        $convertedFrom = $from->format('Y-m-d');
        $to = new \DateTime($to);
        $convertedTo = $to->format('Y-m-d');

        // Make API request to get gold prices from https://api.nbp.pl/api/cenyzlota/{startDate}/{endDate}
        $response = $this->client->request(
            'GET',
            'https://api.nbp.pl/api/cenyzlota/' . $convertedFrom . '/' . $convertedTo . '/',
            [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );

        // Convert the response to an array
        $content = $response->toArray();

        // Get first object with 'from' date from the response array
        $from = $content[0]['data'];

        // Get last object with 'to' date from the response array
        $to = end($content)['data'];

        // Convert 'from' and 'to' dates to the iso8601 format
        $timeZone = new \DateTimeZone('Europe/Warsaw');
        $from = new \DateTime($from, $timeZone);
        $from = $from->format('Y-m-d\TH:i:sP');
        $to = new \DateTime($to, $timeZone);
        $to = $to->format('Y-m-d\TH:i:sP');

        // Calculate the average gold price
        $avg = array_sum(array_column($content, 'cena')) / count($content);

        // Round the average gold price to 2 decimal places
        $avg = round($avg, 2);

        // Return data
        return [$from, $to, $avg];
    }
}