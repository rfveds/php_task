<?php
/**
 * GoldController
 */

namespace App\Controller;

use App\Service\GoldServiceInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GoldController
 */
class GoldController extends AbstractController
{
    /**
     * Gold service
     */
    private GoldServiceInterface $goldService;

    /**
     * GoldController constructor.
     */
    public function __construct(GoldServiceInterface $goldService)
    {
        $this->goldService = $goldService;
    }

    /**
     * Index action.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/api/gold', name: 'app_gold', methods: ['POST'])]
    public function index(Request $request): JsonResponse
    {
        // Get the "from" and "to" parameters from the request
        $data = $request->request->all();
        $from = $data['from'];
        $to = $data['to'];

        // Get data from the service if throws an exception return 400 Bad Request
        try {
            [$from, $to, $avg] = $this->goldService->getGoldPrices($from, $to);
            // Return a JSON response with the average gold price
            return $this->json([
                'from' => $from,
                'to' => $to,
                'avg' => $avg,
            ]);
        } catch (Exception $e) {
            return $this->json([
                'error' => $e->getMessage(),
            ], 400);
        }
    }
}